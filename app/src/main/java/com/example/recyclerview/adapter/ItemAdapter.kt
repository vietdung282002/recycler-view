package com.example.recyclerview.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.example.recyclerview.R
import com.example.recyclerview.model.Affirmation

class ItemAdapter(private val context: Context, private val dataset: List<Affirmation>):
    RecyclerView.Adapter<ViewHolder>() {

    class VerticalViewHolder(itemView: View): ViewHolder(itemView){
        val textView: TextView = itemView.findViewById(R.id.item_title)
        val imageView: ImageView = itemView.findViewById(R.id.item_image)
    }

    class HorizonViewHolder(itemView: View): ViewHolder(itemView){
        val textView: TextView = itemView.findViewById(R.id.item_title)
        val imageView: ImageView = itemView.findViewById(R.id.item_image)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return when (viewType){
            0 -> VerticalViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.vertical,parent,false)
            )
            else -> HorizonViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.horizontal,parent,false)
            )
        }
    }

    override fun getItemCount(): Int {
        return  dataset.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = dataset[position]
        if(getItemViewType(position) == 0){
            val verHolder = holder as VerticalViewHolder
            verHolder.textView.text = context.resources.getString(item.stringResourceId)
            verHolder.imageView.setImageResource(item.imageResourceId)
        }else{
            val horHolder = holder as HorizonViewHolder
            horHolder.textView.text = context.resources.getString(item.stringResourceId)
            horHolder.imageView.setImageResource(item.imageResourceId)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (position < 5) 0 else 1
    }
}

